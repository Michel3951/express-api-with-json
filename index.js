const express = require('express')
const app = express()
const { port } = require('./config.json')

app.get('/', async (req, res) => {
  const data = fetch('https://www.reddit.com/r/AskReddit.json')
    .then((response) => {
      return response.json() // Zet de response om naar een JSON object
    })
    .then((json) => {
      // Transformeer de data
      const newData = json.data.children.map((post) => {
        const data = post.data;

        // Alleen teruggeven wat we nodig hebben
        return {
          title: data.title,
          author: data.author,
          upvotes: data.ups,
          downvotes: data.downs,
          awards: data.total_awards_received
        }
      })

      res.send(newData) // Stuur de nieuwe data terug naar de client
    })
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})